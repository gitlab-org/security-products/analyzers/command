# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

The release dates can be found on the [releases page](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases).

## v3.1.0
- Execute `TruncateTextFields` in `run` command (!55)

## v3.0.0
- Support remote rulesets by injecting config into Analyze and Convert functions (!50)

## v2.4.0
- Update `report` from `v4.1.0` to `v4.4.0` (!49)
- Update `ruleset` from `v1.4.0` to `v2.0.8` (!49)

## v2.3.0
- Remove `dependency_files` from json output (!54)

## v2.2.0
- Add support for parsing CLI argument sequences from an input string (!48)

## v2.1.0
- Bump `report` to `v4.1.0` (!47)
- Bump command version to `command/v2` in `go.mod` to fix inconsistency with git tag (!47)

## v2.0.0
- Bump `report` to `v4.0.0` (!46)

## v1.10.3
- Bump `report` to `v3.22.1` (!45)

## v1.10.2
- Bump `report` to `v3.19.0` (!44)

## v1.10.1
- Always produce JSON reports at the root of the target project (!42)
- Bump `report` to `v3.17.0` (!42)

## v1.10.0
- Set mode 0644 instead of 0600 for the report artifact (!41)

## v1.9.2
- Update common to `v3.2.1` to fix gotestsum cmd (!39)

## v1.9.1
- Improve format in which string slice env vars are logged (!37)

## v1.9.0
- Log env vars specified as command flags in the debug level (!36)

## v1.8.2
- Improve log messages related to repository-wide scanning (!33)

## v1.8.1
- Upgrade `report` package to `v3.12.2` to remove `common/v2` from dependencies (!34)

## v1.8.0
- Upgrade the `common` package to `v3.2.0` to support globstar patterns (!32)

## v1.7.1
- Log a message asking users to enable `debug` mode when the analyzer fails and the log level is set to `info` or lower (!31)

## v1.7.0
- Add `scan.analyzer` field to the security report (!25)
- Report analyzer version in the log, instead of the scanner version (!25)

## v1.6.1
- Add Go version information to the version string (!23)

## v1.6.0
- Adds support for overriding rules (!22)

## v1.5.1
- Fix run to write reports to the target dir (!20)

## v1.5.0
- Add `ArtifactNameClusterImageScanning` for Cluster Image Scanning reports (!18)

## v1.4.1
- Fix location line condition (!16)
- Bump report version to v3.3.0 (!15)

## v1.4.0
- JSON artifacts generation optimizations (!14)
  - avoid redundant `report.Vulnerability.Location.LineEnd`
  - don't report vulnerabilities affecting files that are not in the git repository
  - optimizations are enabled by default and are disabled when the environment variable `ANALYZER_OPTIMIZE_REPORT` is set to `0`

## v1.3.0
- Do not indent JSON artifacts for convert command per default (!13)

## v1.2.0
- Do not indent JSON artifacts for run command per default (!12)

## v1.1.3
- Fixed vulnerabilities found by gosec (!10)

## v1.1.2
- Fixed an error that occured when rulesets were not configured (!8)

## v1.1.1
### Changed

- Clarify in log output that all directories are scanned by analyzers with `analyzeAll` enabled (!6)

## v1.1.0
### Changed

- Update subcommands to rely on `analyzers/report/v2`, switching `Issue` struct to `Vulnerability` (!3)

## v1.0.1
### Changed

- Safe handle command file closures (!2)

## v1.0.0
### Added

- Add command package for secure analyzer support (!1)
