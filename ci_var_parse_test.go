package command

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestParseCLIArg(t *testing.T) {
	testCases := []struct {
		testCase        string
		input           string
		wantFlags       []CIVarBlock
		wantInvalidArgs []string
	}{

		{
			testCase: "args with a space separated value prefixed with a -/--",
			input:    "-arg1 value1 --arg2 value2",
			wantFlags: []CIVarBlock{
				{"-arg1", "value1", false},
				{"--arg2", "value2", false},
			},
		},
		{
			testCase: "args with/without a space separated value prefixed with --",
			input:    "--arg1 value1 --flag1 --flag2 ",
			wantFlags: []CIVarBlock{
				{"--arg1", "value1", false},
				{"--flag1", "", true},
				{"--flag2", "", true},
			},
		},
		{
			testCase: "args with a equal(=) separated value prefixed with a -/--",
			input:    "-arg1=value1 --arg2=value2",
			wantFlags: []CIVarBlock{
				{"-arg1", "value1", false},
				{"--arg2", "value2", false},
			},
		},
		{
			testCase: "args and flags with/without a space/= separated value prefixed with --",
			input:    "--arg1 value1 --arg2=value2 --flag1 -arg4:test=value4",
			wantFlags: []CIVarBlock{
				{"--arg1", "value1", false},
				{"--arg2", "value2", false},
				{"--flag1", "", true},
				{"-arg4:test", "value4", false},
			},
		},
		{
			testCase:        "invalid value2 with valid arg1",
			input:           "-arg1=value1 value2",
			wantFlags:       []CIVarBlock{{"-arg1", "value1", false}},
			wantInvalidArgs: []string{"value2"},
		},
		{
			testCase:        "invalid value1 and value2",
			input:           "value1 value2",
			wantInvalidArgs: []string{"value1", "value2"},
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.testCase, func(t *testing.T) {
			gotFlags, gotInvalid := ParseCIVar(tc.input)
			require.Len(t, gotFlags, len(tc.wantFlags))
			require.Len(t, gotInvalid, len(tc.wantInvalidArgs))
			require.EqualValues(t, tc.wantInvalidArgs, gotInvalid)
			for _, got := range gotFlags {
				var foundEqual bool
				for _, want := range tc.wantFlags {
					if reflect.DeepEqual(got, want) {
						foundEqual = true
						break
					}
				}
				require.Truef(t, foundEqual, "mismatch expectation for input: %#v", got)
			}
		})
	}
}
