package command

import (
	"bytes"
	"errors"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v5"
	ruleset "gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3"
)

func TestNewApp_ErrorHandler(t *testing.T) {
	failErr := errors.New("something went wrong")
	want := errOnExitMsg(failErr)

	tcs := []struct {
		name    string
		cfg     Config
		wantErr error
		wantMsg string
		preset  func()
	}{
		{
			name:    "analyze function failure",
			cfg:     newFailCfg("analyze", failErr),
			wantErr: failErr,
			wantMsg: want,
		},
		{
			name:    "match function failure",
			cfg:     newFailCfg("match", failErr),
			wantErr: failErr,
			wantMsg: want,
		},
		{
			name:    "convert function failure",
			cfg:     newFailCfg("convert", failErr),
			wantErr: failErr,
			wantMsg: want,
		},
		{
			name:    "serializer function failure",
			cfg:     newFailCfg("serialize", failErr),
			wantErr: failErr,
			wantMsg: want,
		},
		{
			name:    "no function failure",
			cfg:     newDefaultCfg(),
			wantErr: nil,
			wantMsg: "",
		},
		{
			name:    "function failure with SECURE_LOG_LEVEL=debug",
			cfg:     newFailCfg("analyze", failErr),
			wantErr: failErr,
			wantMsg: "",
			preset: func() {
				// assumption based on logutil.init()
				log.SetLevel(log.DebugLevel)
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			if tc.preset != nil {
				tc.preset()
			}
			buf := bytes.NewBuffer(nil)
			log.SetOutput(buf)
			app := NewApp(report.AnalyzerDetails{})
			app.Commands = []*cli.Command{Run(tc.cfg)}
			err := app.Run([]string{"analyzer", "run"})
			logBuffer := buf.String()

			assert.Equal(t, tc.wantErr, err)
			assert.Contains(t, logBuffer, tc.wantMsg)
		})
	}
}

func newDefaultCfg() Config {
	return Config{
		Analyze:    newAnalyzeFunc(nil),
		Match:      newMatchFunc(nil),
		Convert:    newConvertFunc(nil, nil),
		Serializer: SerializeJSONToFile,
		LoadRulesetConfig: func(_ string) (*ruleset.Config, error) {
			return &ruleset.Config{}, nil
		},
	}
}

// newFailCfg returns Config that has failing failureFunc function with err
func newFailCfg(failureFunc string, err error) Config {
	cfg := newDefaultCfg()
	switch failureFunc {
	case "analyze":
		cfg.Analyze = newAnalyzeFunc(err)
	case "match":
		cfg.Match = func(_ string, _ os.FileInfo) (bool, error) {
			return false, err
		}
	case "convert":
		cfg.Convert = newConvertFunc(err, nil)
	case "serialize":
		cfg.Serializer = func(_ *report.Report, _, _ string, _, _ bool) error {
			return err
		}
	case "ruleset":
		cfg.LoadRulesetConfig = func(_ string) (*ruleset.Config, error) {
			return nil, err
		}
	}
	return cfg
}
