package command

import (
	"io"
	"os"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v3/cacert"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v5"
	ruleset "gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3"
)

// ConvertFunc is a type for a function that parses the analyzer binary raw output
// and converts it into the report data structure provided by the library.
type ConvertFunc func(input io.Reader, prependPath string, rulesetConfig *ruleset.Config) (*report.Report, error)

const flagPrependPath = "prepend-path"

// Convert returns a cli sub-command that converts the analyzer output into an artifact.
func Convert(cfg Config) *cli.Command {
	flags := []cli.Flag{
		&cli.StringFlag{
			Name:  flagPrependPath,
			Usage: "Path prepended to paths of affected files",
			Value: ".",
		},
		&cli.BoolFlag{
			Name:    flagIndent, // #nosec
			Usage:   "indent",
			EnvVars: []string{EnvVarIndentReport},
			Value:   false,
		},
		&cli.BoolFlag{
			Name:    flagOptimize, // #nosec
			Usage:   "optimize",
			EnvVars: []string{EnvVarOptimizeReport},
			Value:   true,
		},
	}

	return &cli.Command{
		Name:      "convert",
		Aliases:   []string{"c"},
		Usage:     "Convert analyzer output to a compatible artifact",
		ArgsUsage: "<input>",
		Flags:     append(cacert.NewFlags(), flags...),
		Action: func(c *cli.Context) error {
			// check args
			if c.Args().Len() != 1 {
				if err := cli.ShowSubcommandHelp(c); err != nil {
					return err
				}
				return errInvalidArgs
			}

			// import CA bundle
			if err := cacert.Import(c, cfg.CACertImportOptions); err != nil {
				return err
			}

			// open input file
			input := filepath.Clean(c.Args().First())
			reader, err := os.Open(input)
			if err != nil {
				return err
			}
			defer func() {
				if err := reader.Close(); err != nil {
					log.Error(err)
				}
			}()

			// convert output to report
			log.Info("Creating report")
			report, err := cfg.Convert(reader, c.String(flagPrependPath), cfg.RulesetConfig)
			if err != nil {
				return err
			}

			report.Scan.Analyzer = cfg.Analyzer
			report.Scan.Scanner = cfg.Scanner
			report.Scan.Type = cfg.ScanType

			return SerializeJSONToWriter(report, c.App.Writer, flagPrependPath, c.Bool("indent"), c.Bool("optimize"))
		},
	}
}
