module gitlab.com/gitlab-org/security-products/analyzers/command/v3

go 1.15

require (
	github.com/sirupsen/logrus v1.9.3
	github.com/stretchr/testify v1.9.0
	github.com/urfave/cli/v2 v2.27.1
	gitlab.com/gitlab-org/security-products/analyzers/common/v3 v3.2.1
	gitlab.com/gitlab-org/security-products/analyzers/report/v5 v5.1.0
	gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3 v3.0.0
)
