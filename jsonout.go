package command

import (
	"bytes"
	"encoding/json"
	"io"
	"os"
	"os/exec"
	"strings"

	log "github.com/sirupsen/logrus"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v5"
)

// SerializerFunc function for serializing and optimizing report output
type SerializerFunc func(report *report.Report, artifactPath string, flagPrependPath string, indent bool, optimize bool) error

// SerializeJSONToFile will write a report to a path
func SerializeJSONToFile(report *report.Report, artifactPath string, flagPrependPath string, indent bool, optimize bool) error {
	/* #nosec */
	artifactFile, err := os.OpenFile(artifactPath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
	if err != nil {
		return err
	}
	return SerializeJSONToWriter(report, artifactFile, flagPrependPath, indent, optimize)
}

// SerializeJSONToWriter optimizes the report based on the optimize flag
// and then encodes the report
func SerializeJSONToWriter(report *report.Report, writer io.Writer, flagPrependPath string, indent bool, optimize bool) error {
	outReport := report
	enc := json.NewEncoder(writer)
	if indent {
		enc.SetIndent("", "  ")
	}

	if optimize {
		var err error
		outReport, err = EliminateRedundancies(outReport, flagPrependPath)
		if err != nil {
			return err
		}
	}

	return enc.Encode(outReport)
}

// EliminateRedundancies is a function that helps to remove redundancies
// automatically from reports these optimizations include:
// - only include findings that refer to (git) tracked files
// - remove redundant LineEnd information
func EliminateRedundancies(notOptimized *report.Report, flagPrependPath string) (*report.Report, error) {
	log.Debug("Optimizing JSON Output")
	trackedFiles := make(map[string]struct{})

	gitPath, err := exec.LookPath("git")
	if err == nil {
		// get a list of all tracked files
		/* #nosec */
		cmd := exec.Command(gitPath, "ls-tree", "--full-tree", "-r", "--name-only", "HEAD")
		var out bytes.Buffer
		var stderr bytes.Buffer
		cmd.Stdout = &out
		cmd.Stderr = &stderr
		cmd.Dir = flagPrependPath

		err = cmd.Run()
		// fail gracefully if the git command fails
		if err == nil {
			for _, f := range strings.Split(out.String(), "\n") {
				// do not add empty string to the list of tracked files
				if f == "" {
					continue
				}
				trackedFiles[f] = struct{}{}
			}
		}
	} else {
		// proceed without filtering
		log.Warn("Could not detect git executable")
	}

	optimized := []report.Vulnerability{}

	for _, vuln := range notOptimized.Vulnerabilities {
		_, included := trackedFiles[vuln.Location.File]

		// do not add entries to the report for untracked files
		if len(trackedFiles) > 0 && !included {
			continue
		}

		linediff := vuln.Location.LineEnd - vuln.Location.LineStart
		// LineEnd == 0 happens if the LineEnd is undefined in the JSON file
		if linediff < 0 && vuln.Location.LineEnd != 0 {
			// LineEnd is defined and LineStart > LineEnd
			log.Fatalf("Invalid vulnerability location: LineStart > LineEnd")
		} else if linediff == 0 {
			// LineEnd == LineStart
			vuln.Location.LineEnd = 0
		}

		optimized = append(optimized, vuln)
	}

	rep := report.NewReport()
	rep.Version = notOptimized.Version
	rep.Vulnerabilities = optimized
	rep.Remediations = notOptimized.Remediations
	rep.Scan = notOptimized.Scan
	rep.Analyzer = notOptimized.Analyzer
	rep.Config = notOptimized.Config

	return &rep, nil
}
