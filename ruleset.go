package command

import (
	"gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3"
)

// RulesetConfigFunc is a type for a function that loads the analyzer's ruleset
// from the project dir and returns the Ruleset.
type RulesetConfigFunc func(projectPath string) (*ruleset.Config, error)
