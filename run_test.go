package command

import (
	"bytes"
	"errors"
	"io"
	"os"
	"path"
	"path/filepath"
	"strings"
	"testing"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v3/search"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v5"
	ruleset "gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3"
)

func newRunCmd(cfg Config, buf *bytes.Buffer) cli.App {
	log.SetOutput(buf)
	app := cli.NewApp()
	app.Name = "run"
	app.Usage = "Test run command"
	app.Commands = []*cli.Command{Run(cfg)}

	return *app
}

func newAnalyzeFunc(err error) AnalyzeFunc {
	return AnalyzeFunc(func(_ *cli.Context, _ string, _ *ruleset.Config) (io.ReadCloser, error) {
		return io.NopCloser(bytes.NewBuffer([]byte(""))), err
	})
}

func newConvertFunc(err error, reportData *report.Report) ConvertFunc {
	return ConvertFunc(func(_ io.Reader, _ string, _ *ruleset.Config) (*report.Report, error) {
		if reportData != nil {
			return reportData, err
		}

		newReport := report.NewReport()
		return &newReport, err
	})
}

func newMatchFunc(err error) search.MatchFunc {
	return func(_ string, _ os.FileInfo) (bool, error) {
		return true, err
	}
}

func newLoadConfigFunc(err error) RulesetConfigFunc {
	return func(string) (*ruleset.Config, error) {
		rulesetConfig := ruleset.Config{}

		return &rulesetConfig, err
	}
}

func TestRunErrorsOnSubCommands(t *testing.T) {
	tcs := []struct {
		name          string
		analyze       AnalyzeFunc
		convert       ConvertFunc
		match         search.MatchFunc
		ruleset       RulesetConfigFunc
		wantErr       error
		wantLogLevel  string
		wantDetecting string
		wantFound     string
		wantLoading   string
		wantRunning   string
		wantCreating  string
	}{
		{
			name:          "successful run",
			analyze:       newAnalyzeFunc(nil),
			convert:       newConvertFunc(nil, nil),
			match:         newMatchFunc(nil),
			ruleset:       newLoadConfigFunc(nil),
			wantErr:       nil,
			wantLogLevel:  "[INFO]",
			wantDetecting: "▶ Detecting project",
			wantFound:     "▶ Analyzer found a supported project at",
			wantRunning:   "▶ Running analyzer",
			wantLoading:   "▶ Loading ruleset for",
			wantCreating:  "▶ Creating report",
		},
		{
			name:          "when the analyze function fails",
			analyze:       newAnalyzeFunc(errors.New("oh noez")),
			convert:       newConvertFunc(nil, nil),
			match:         newMatchFunc(nil),
			ruleset:       newLoadConfigFunc(nil),
			wantErr:       errors.New("oh noez"),
			wantLogLevel:  "[INFO]",
			wantDetecting: "▶ Detecting project",
			wantFound:     "▶ Analyzer found a supported project at",
			wantLoading:   "▶ Loading ruleset for",
			wantRunning:   "▶ Running analyzer",
		},
		{
			name:          "when the convert function fails",
			analyze:       newAnalyzeFunc(nil),
			convert:       newConvertFunc(errors.New("oh noez"), nil),
			match:         newMatchFunc(nil),
			ruleset:       newLoadConfigFunc(nil),
			wantErr:       errors.New("oh noez"),
			wantLogLevel:  "[INFO]",
			wantDetecting: "▶ Detecting project",
			wantFound:     "▶ Analyzer found a supported project at",
			wantLoading:   "▶ Loading ruleset for",
			wantRunning:   "▶ Running analyzer",
		},
		{
			name:          "when the ruleset config function fails",
			analyze:       newAnalyzeFunc(nil),
			convert:       newConvertFunc(nil, nil),
			match:         newMatchFunc(nil),
			ruleset:       newLoadConfigFunc(errors.New("oh noez")),
			wantErr:       errors.New("oh noez"),
			wantLogLevel:  "[INFO]",
			wantDetecting: "▶ Detecting project",
			wantFound:     "▶ Analyzer found a supported project at",
			wantLoading:   "▶ Loading ruleset for",
			wantRunning:   "", //It shouldn't get this far
		},
		// This test has never worked because `search.Run()` doesn't first check
		// the error returned from the Match function:
		//  https://gitlab.com/gitlab-org/security-products/analyzers/common/-/blob/6053a5e2e6cfea1f377e6a83ed0e0a15610d2968/search/search.go#L37-46
		//{
		//	Name:          "when the match function fails",
		//	Analyze:       newAnalyzeFunc(nil),
		//	Convert:       newConvertFunc(nil, nil),
		//	Match:         newMatchFunc(errors.New("oh noez")),
		//	wantErr:       errors.New("oh noez"),
		//	wantLogLevel:  "[INFO]",
		//	wantDetecting: "▶ Detecting project",
		//	wantFound:     "▶ Analyzer found a supported project at",
		//	wantRunning:   "▶ Running analyzer",
		//},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			cfg := Config{
				Analyze:           tc.analyze,
				Convert:           tc.convert,
				Match:             tc.match,
				LoadRulesetConfig: tc.ruleset,
			}

			buf := new(bytes.Buffer)
			app := newRunCmd(cfg, buf)

			assert.Equal(t, tc.wantErr, app.Run([]string{"analyzer", "run"}))

			actualOutput := strings.TrimSpace(buf.String())

			assert.Contains(t, actualOutput, tc.wantLogLevel)
			assert.Contains(t, actualOutput, tc.wantDetecting)
			assert.Contains(t, actualOutput, tc.wantFound)
			assert.Contains(t, actualOutput, tc.wantRunning)
			assert.Contains(t, actualOutput, tc.wantLoading)
			assert.Contains(t, actualOutput, tc.wantCreating)
		})
	}
}

func TestRunAnalyzeSerializeJSONToFileErr(t *testing.T) {
	serializer := SerializerFunc(func(_ *report.Report, artifactPath string, _ string, _ bool, _ bool) error {
		// Ensure artifactPath is a full path to artifact and not just artifact name
		want := "/" + ArtifactNameSAST
		assert.Contains(t, artifactPath, want, "artifactPath should be a full path to the artifact")
		return nil
	})

	cfg := Config{
		Analyze:           newAnalyzeFunc(nil),
		Convert:           newConvertFunc(nil, nil),
		Match:             newMatchFunc(nil),
		Serializer:        serializer,
		LoadRulesetConfig: newLoadConfigFunc(nil),
	}

	buf := new(bytes.Buffer)
	app := newRunCmd(cfg, buf)

	assert.NoError(t, app.Run([]string{"analyzer", "run"}))
}

func TestRunDebugLogEnvVars(t *testing.T) {
	tcs := []struct {
		name           string
		logLevel       log.Level
		envVars        map[string]string
		expectedLogs   []string
		unexpectedLogs []string
	}{
		{
			name:     "Debug-EnvVarsSet",
			logLevel: log.DebugLevel,
			envVars: map[string]string{
				"ANALYZER_TARGET_DIR":       "/tmp/target",
				"CI_PROJECT_DIR":            "/tmp/project",
				"SEARCH_IGNORE_HIDDEN_DIRS": "false",
				"SAST_EXCLUDED_PATHS":       "a,b",
			},
			expectedLogs: []string{
				"ANALYZER_TARGET_DIR,CI_PROJECT_DIR=/tmp/target",
				"SEARCH_IGNORE_HIDDEN_DIRS=false",
				"SAST_EXCLUDED_PATHS=a,b",
			},
		},
		{
			name:     "Debug-AnalyzerTargetDirNotSet",
			logLevel: log.DebugLevel,
			envVars: map[string]string{
				"CI_PROJECT_DIR":            "/tmp/project",
				"SEARCH_IGNORE_HIDDEN_DIRS": "true",
			},
			expectedLogs: []string{
				"ANALYZER_TARGET_DIR,CI_PROJECT_DIR=/tmp/project",
				"SEARCH_IGNORE_HIDDEN_DIRS=true",
			},
		},
		{
			name:     "Info",
			logLevel: log.InfoLevel,
			unexpectedLogs: []string{
				"ANALYZER_TARGET_DIR,CI_PROJECT_DIR=",
				"SEARCH_IGNORE_HIDDEN_DIRS=",
			},
		},
		{
			name:     "Trace-EnvVarsNotSet",
			logLevel: log.TraceLevel,
			expectedLogs: []string{
				"ANALYZER_TARGET_DIR,CI_PROJECT_DIR=",
				"SEARCH_IGNORE_HIDDEN_DIRS=true",
			},
		},
	}

	cfg := Config{
		Analyze:           newAnalyzeFunc(nil),
		Convert:           newConvertFunc(nil, nil),
		Match:             newMatchFunc(nil),
		LoadRulesetConfig: newLoadConfigFunc(nil),
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			log.SetLevel(tc.logLevel)

			// set env vars
			for envVar, val := range tc.envVars {
				t.Setenv(envVar, val)
			}

			buf := new(bytes.Buffer)
			app := newRunCmd(cfg, buf)
			err := app.Run([]string{"analyzer", "run"})
			require.NoError(t, err)

			got := strings.TrimSpace(buf.String())

			for _, expectedLog := range tc.expectedLogs {
				assert.Contains(t, got, expectedLog)
			}

			for _, unexpectedLog := range tc.unexpectedLogs {
				assert.NotContains(t, got, unexpectedLog)
			}
		})
	}

	// reset to default
	log.SetLevel(log.DebugLevel)
}

func TestArtifactPath(t *testing.T) {
	cwd, err := os.Getwd()
	require.NoError(t, err)

	targetProjectDir := path.Join(cwd, "testdata", "artifactPath")
	expectedReportPath := path.Join(targetProjectDir, "gl-sast-report.json")
	defer func() {
		// Don't check error in case the file was never created in the first place.
		os.Remove(expectedReportPath)
	}()

	t.Setenv("CI_PROJECT_DIR", targetProjectDir)

	cfg := Config{
		Analyze: newAnalyzeFunc(nil),
		Convert: newConvertFunc(nil, nil),
		Match: func(_ string, info os.FileInfo) (bool, error) {
			if filepath.Ext(info.Name()) == ".rb" {
				return true, nil
			}
			return false, nil
		},
		LoadRulesetConfig: newLoadConfigFunc(nil),
	}

	buf := new(bytes.Buffer)
	app := newRunCmd(cfg, buf)

	require.NoError(t, app.Run([]string{"analyzer", "run"}))

	_, err = os.Stat(expectedReportPath)
	require.NoError(t, err, "should produce gl-sast-report.json in the project root")
}

func TestRunSucceedsEvenWhenNoLoadRulesetConfigFuncIsSupplied(t *testing.T) {
	t.Run("Run does not fail if no LoadRulesetConfigFunc is set", func(t *testing.T) {
		cfg := Config{
			Analyze:           newAnalyzeFunc(nil),
			Convert:           newConvertFunc(nil, nil),
			Match:             newMatchFunc(nil),
			LoadRulesetConfig: nil,
		}

		buf := new(bytes.Buffer)
		app := newRunCmd(cfg, buf)

		require.NoError(t, app.Run([]string{"analyzer", "run"}))
	})
}

func TestRunTruncatesTextFields(t *testing.T) {
	reportWithLongName := report.Report{
		Vulnerabilities: []report.Vulnerability{
			{
				Name: "Vulnerability with short name",
			},
			{
				Name: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
			},
		},
	}

	reportWithTruncatedName := report.Report{
		Vulnerabilities: []report.Vulnerability{
			{
				Name: "Vulnerability with short name",
			},
			{
				Name: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolo...",
			},
		},
	}

	cfg := Config{
		Analyze:           newAnalyzeFunc(nil),
		Convert:           newConvertFunc(nil, &reportWithLongName),
		Match:             newMatchFunc(nil),
		LoadRulesetConfig: nil,
	}

	buf := new(bytes.Buffer)
	app := newRunCmd(cfg, buf)

	require.NoError(t, app.Run([]string{"analyzer", "run"}))
	require.Equal(t, reportWithTruncatedName.Vulnerabilities, reportWithLongName.Vulnerabilities)
}
