package command

import (
	"bytes"
	"errors"
	"os"
	"testing"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v3/search"
)

var (
	lastExitCode = 0
	fakeOsExiter = func(rc int) {
		lastExitCode = rc
	}
	fakeErrWriter = &bytes.Buffer{}
)

func init() {
	cli.OsExiter = fakeOsExiter
	cli.ErrWriter = fakeErrWriter
}

func TestSearch(t *testing.T) {
	successfulMatcher := func(filename string) search.MatchFunc {
		return func(_ string, info os.FileInfo) (bool, error) {
			return info.Name() == filename, nil
		}
	}

	tcs := []struct {
		name         string
		match        search.MatchFunc
		runArgs      []string
		wantOut      string
		wantErrStr   string
		wantExitCode int
	}{
		{
			name:    "successfully finds a Java manifest",
			match:   successfulMatcher("pom.xml"),
			runArgs: []string{"analyzer", "search", "testdata/search"},
			wantOut: "testdata/search",
		},
		{
			name:    "successfully finds a C file",
			match:   successfulMatcher("main.c"),
			runArgs: []string{"analyzer", "search", "testdata/search"},
			wantOut: "testdata/search",
		},
		{
			name:    "successfully finds a directory named 'c'",
			match:   successfulMatcher("c"),
			runArgs: []string{"analyzer", "search", "testdata/search"},
		},
		{
			name: "when there's a match error",
			match: func(_ string, _ os.FileInfo) (bool, error) {
				return false, errors.New("match filename")
			},
			runArgs:    []string{"analyzer", "search", "testdata/search"},
			wantErrStr: "match filename",
		},
		{
			name: "when there's no match",
			match: func(_ string, _ os.FileInfo) (bool, error) {
				return false, nil
			},
			runArgs:      []string{"analyzer", "search", "testdata/search"},
			wantErrStr:   "No match in testdata/search",
			wantExitCode: 3,
		},
		{
			name: "when invalid run arguments are provided",
			match: func(_ string, _ os.FileInfo) (bool, error) {
				return false, nil
			},
			runArgs:      []string{"analyzer", "search"},
			wantErrStr:   "Invalid arguments",
			wantExitCode: 2,
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			buf := new(bytes.Buffer)
			log.SetOutput(buf)
			app := cli.NewApp()
			app.Name = "search"
			app.Usage = "Test search command"
			app.Commands = []*cli.Command{Search(Config{Match: tc.match})}

			err := app.Run(tc.runArgs)
			if tc.wantErrStr != "" {
				require.EqualError(t, err, tc.wantErrStr)
			}

			assert.Contains(t, buf.String(), tc.wantOut)

			assert.Equal(t, tc.wantExitCode, lastExitCode, "exit code should be correct")
		})
	}
}
